# Purpose

This repository hosts the latest (as of October 2023) Ubuntu Touch 20.04 (Focal) **development** image builder for Pine64 devices (PinePhone, PinePhone Pro, PineTab, and PineTab2).

As of 2023-10-03, the work here has been merged into the [upstream Ubuntu Touch image builder](https://gitlab.com/ubports/development/core/rootfs-builder-debos/)- ([MR #108](https://gitlab.com/ubports/development/core/rootfs-builder-debos/-/merge_requests/108), [MR #109](https://gitlab.com/ubports/development/core/rootfs-builder-debos/-/merge_requests/109), [MR #110](https://gitlab.com/ubports/development/core/rootfs-builder-debos/-/merge_requests/110)).
Since then, Pine64 images are built daily in the [Ubuntu Touch CI](https://ci.ubports.com) by the [focal-hybris-rootfs-arm64 master job](https://ci.ubports.com/job/focal-hybris-rootfs-arm64/job/master).

Clicking on the *Last Successful Artifacts* link in the Jenkins job will take you to a page where you can download the image (the *.raw.xz* file) for your device.

You can now flash this image on an SD card or your Pine device and boot it.

# Note
[v0.10](https://gitlab.com/ook37/pinephone-pro-debos/-/releases/v0.10) was the last downstream release of Pine64 on Ubuntu Touch 20.04. It will therefore be the last one to be hosted on this GitLab page.
The Help Guide will remain to exist for archival purposes. Issues can be opened, but do not expect any more changes to this repository. Those issues opened should be for helping each other out when problems arise, as well as possibly assisting with the upstreaming process.

Our next release will come from upstream UBPorts source directly, if all goes well. This will come with a significant change to how the filesystems have been running - they will go from r/w images with standard booting to a more secure a/b boot with r/o images. This will be a larger group effort, in which I will be able to hand over a lot of the reigns, so I want to thank everyone who has supported me as I've solo-spearheaded this project for the last 8 months. It's been spectacular!

**TL;DR** 
The *Unoffical* portion of  the Pine64 on Ubuntu Touch 20.04 has come to an close. Keep an eye out in the UBPorts newsletters for when we have imported and gone *Official*!

----

**PinePhone (non-Pro) users:** make sure you have followed the first section of steps at https://ubports.com/blog/ubports-news-1/post/pinephone-and-pinephone-pro-3889 before installing!

**Strongly Recommended Modem Firmware:**
- [Biktorgj's Open Modem Firmware v0.7.2](https://github.com/the-modem-distro/pinephone_modem_sdk/releases/tag/0.7.2)
  - ADSP Version 30.006.30.006

##### [Help Guide/Wiki](https://gitlab.com/ook37/pinephone-pro-debos/-/wikis/Help-Guide)

### PinePhone (Pro) + PineTab/2: Ubuntu Touch 20.04 Port
_features with a star are for phones only_

| Currently supported features: | Features partially working: | Features currently not working: |
|-------------------------------|-----------------------------|---------------------------------|
|  Boot into full UI            |  SSH                        |  Cameras (front and back)       |
|  Touchscreen                  |  Wired external monitor     |  Proximity sensor               |
|  On-Screen Keyboard           |  GPS                        |
|  SD card detection/access     |                             |
|  Online charging              |                             |
|  RTC time                     |                             |  
|  Shutdown/reboot              |                             |  
|  Wifi                         |                             
|  Bluetooth (FTP, Audio) _(PineTab2 needs port)_      |
|  Flight Mode                  |
|  Apparmor patches             |
|  PinePhone Keyboard*          |
|  Video playback               | 
|  Manual brightness            |
|  Notification LED*            |
|  Waydroid (mainline)          |
|  UART/serial console          |
|  Modem (Internet, SMS)*       |
|  On-device audio              |
|  Charging indicator LED*      |
|  Ethernet                     |
|  Flashlight/torch             |
|  Accelerometer/gyroscope      |

### Debos build recipes

This uses debos to build (sdcard) images for devices

Supported devices:
 - Pine64
    - PinePhone Don't be evil development kit
    - PinePhone Developer Batch (1.0)
    - PinePhone Braveheart (1.1)
    - PinePhone (1.2)
    - PinePhone Pro
    - PineTab
    - PineTab2

**NOTE:** Tow-Boot is ***required*** for PinePhone + Pro!

To build images locally, run:
```
./debos-docker -m 5G {device}.yaml
```

NOTE: this uses 5G of ram, you might be able to decreases this, but may run into
space issues if you do.

You will need docker installed and working.

If this is not working...

Prerequisites
-------------

* enabled virtualization:

  - check if you have `kvm` device node:

    ```
    ls /dev/kvm
    ```

  If not, enable virtualization in BIOS and check again.

  - add your user to kvm group:

    ```
    usermod -a -G kvm <your-user-name>
    ```

   Visit [linux-kvm FAQ](https://www.linux-kvm.org/page/FAQ) for more informations.

* `binfmt_misc` module loaded on the host machine:

```
modprobe binfmt_misc
```

Pull Docker image
-----------------

```
docker pull jbbgameich/debos-docker
```

Running
-------

```
./run.sh DEBOS_PARAMETERS
```

> Visit [debos repo](https://github.com/go-debos/debos) for tool usage

For easy access from any directory:

```
ln -s $(readlink -f run.sh) ~/bin/debos-docker
debos-docker DEBOS_PARAMETERS

git clone https://gitlab.com/ook37/pinephone-pro-debos.git
cd pinephone-pro-debos
debos-docker -m 5G <device>.yaml
For PinePhone + Pro this means 
debos-docker -m 5G pinephone-unified.yaml
